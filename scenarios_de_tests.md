# 1. Recette technique 
Nous avons mis en place des tests permettant de vérifier le fonctionnement technique de notre applicatif. En effet les différents niveaux de tests que nous vous présentons en suivant nous permettent de contrôler la conformité technique de notre produit. Cela nous permet de vérifier que les fonctionnalités mises en place ont été implémentées correctement, de s'assurer de la qualité de l'applicatif, de la gestion des données, mais aussi que notre produit est protégé des risques que nous avons prévus mais aussi de ceux qui peuvent survenir de façon inattendue. 

## 1.a : Tests unitaire
Chaque développeur contrôle son évolution pas à pas. Pour cela il utilise des tests unitaires. Progressivement, le développeur vérifie que son travail compile bien, qu'il est bien visible sur l'application, sans erreurs ou warning et qu'il correspond à l'User Story.
Nous avons décidé de ne pas tracer ces tests ci. Ils ne sont pas documentés. 
Ces tests ont pour but de contrôler au fur et à mesure le travail du développeur. C'est lui-même qui assure le bon fonctionnement de son travail et a la responsabilité de rendre un code propre et fonctionnel. 

## 1.b : Tests d'acceptances
Tout d'abord grâce à GitLab puis via l'outil PGM nous avons mis en place des tests d'acceptance. Pris en charge par notre chef de projet, puisqu'expérimenté dans les langages utilisés, cela permet une deuxième correction avec commentaires et retours au développeur si besoin est. 

L'outil PGM a été développé par notre chef de projet afin de gérer les actions git allant de la création de branche à la validation. Cela permet une utilisation beaucoup plus simplifiée et claire des actions basiques que nous faisions via gitlab et l'éditeur de code. 

Concernant la partie test de l'application, l'outil d'intégration en continue (Gitlab CI) lance la totalité des tests unitaires afin de valider si l'implémentation est fonctionnelle ou non.

Ces tests nous permettent d'avoir une deuxième lecture du code. Qui plus est cela étant fait par une personne expérimentée, elle peut ainsi aider à la correction ou modification du code, mais aussi donner des conseils et des outils aux développeurs moins expérimentés. Cela permet un meilleur contrôle et une montée en compétence de nos développeurs. 

## 1.c : Tests EndToEnd
Ce sont des tests dit « de bout en bout ». Ils permettent au développeur de vérifier si l'application se comporte comme elle devrait le faire. Nous nous mettons à la place de l'utilisateur afin de dérouler les tests comme si nous utilisions l’application pour faire ce pour quoi elle a été créée. Ceci permet de vérifier le bon fonctionnement du front mais aussi la bonne intégration du back dans l'ensemble. 

Le but de ces tests est de permettre un contrôle de scénarios complexes et complets. Ils permettent de rejouer différentes actions et fonctionnalités en incluant des données différentes. Cela nous est utile pour détecter des problèmes dans les sous-systèmes de l'applicatif et cela nous permet de mieux contrôler la qualité de notre application

Ces tests seront exécutés avant la mise en production. Nous prévoyons une réunion avec le métier afin de créer une fiche d'utilisation type, par utilisateurs. 

# 2. Recette fonctionnelle.
Des tests dit fonctionnels nous permettent de vérifier si notre produit correspond aux besoins du métier, si nous avons pu répondre aux attentes des utilisateurs et si notre applicatif possède bien l'ensemble des fonctionnalités présentes dans le cahier des charges. 

Un premier contrôle est fait lors des tests présentés sous la recette technique, notamment ceux qui s'occupe de vérifier la conformité à l’User Story. 

## 2.a : Démonstration (mode agile)
Au fur est à mesure des livraisons nous avons mis en place des démonstrations qui ont permis à quelques utilisateurs de voir l'avancée de notre applicatif en suivant les fonctionnalités ajoutées. Cela nous permet d'avoir un retour direct du métier et des utilisateurs afin d'ajuster notre applicatif aux besoins et aux demandes de ceux-ci. Cela permet aussi de tester Madera en direct avec des personnes pouvant apporter leur connaissance fonctionnelle du besoin. 

## 2.b : Recettes utilisateurs
Pour la suite, nous prévoyons qu'un panel du métier, venant des différents types d'utilisateurs, puissent tester l'applicatif via des tests de mise en qualification. 
Nous mettrons en place Madera sur un environnement de qualification accessible à ce panel qui pourra tester l'applicatif dans un temps limité. Nous mettrons donc en place des sessions de recettes qui mèneront à des réunions de restitution. Ces dernières permettront aux utilisateurs de faire des retours à l'équipe de développement afin d'améliorer l'applicatif mais aussi de prévoir un correctif si besoin est. 


