# Rapport

## Gestion des tâches du projet
Workflow Git 
La gestion de notre Workflow de développement sera assez similaire au Gitflow. (cf annexe). 
Gestion des tâches (issues) 
Nos tâches seront répertoriées sur la plateforme Gitlab. La méthode de gestion des tâches sera le Kanban. 
  
9 Kanban Gitlab 
Un workflow de gestion des tâches sera prédéfini afin d’homogénéiser la réalisation de celle-ci. 
Saut de page 
Cheminement du workflow de gestion des tâches 
  
(Figure 10 : Workflow gestion des tâches)

**Etape 1 :**
Le développeur devra s’assigner une tâche, créer une nouvelle branch Git correspondant à la tâche précédemment assignée et changera le statut de la tâche par : En cours d’implémentation 

**Etape 2 :**
Une fois l’implémentation de la tâche terminée, le développeur devra créer une Merge Request et changera le statut de la tâche par : A valider 

**Etape 3 :**
Un validateur (un autre développeur) s’assignera la Merge Request et copiera les données de l’implémentation afin de pouvoir analyser celle-ci. Si le validateur poste des remarques à faire remonter et/ou effectue des modifications à apporter, il changera par la suite le statut de la tâche par : Retour. 
Sinon, si aucune remarque n’est à mentionner, le validateur pourra accepter la Merge Request.  

**Etape 4 (si retour sur la Merge Request) :**
Le développeur prendra les remarques en comptes et il changera le statut de la tâche par : A valider 
Retour à l’Etape 3.

Workflow Git 

Pour plus de simplicité dans notre travail de versionning, notre chef de projet à développer un outil de versionning nommé PGM. Cet outil nous permet de mettre à jour, créer une branche, envoyer une MergeRequest ou même traiter cette dernière de manière plus efficace et beaucoup plus simplement que sur GitLab.
 
PGM interface 1

Comme vous pouvez le voir ci-dessus, toutes les tâches projet sont répertoriées dans l’onglet projet parent. D’un simple click sur les boutons grisés que vous pouvez apercevoir à côté, nous pouvons créer une branche, mettre à jour ou vérifier une MergeRequest.
En conclusion cet outil nous fait gagner énormément de temps.

## Politique de sécurisation

### Pas de possibilité de s'inscrire via l'application :

Afin de sécuriser l'application nous n'avons pas créée de fonctionnalité permettant de s'inscrire directement via l'application. La gestion des comptes et des rôles se fait par l'administrateur, cela implique la mise en place d'un processus au sein de l'entreprise lorsque de nouveaux acteurs ; commerciaux ou personnel du bureau d'étude veulent s'inscrire. Cela permet un meilleur contrôle des utilisateurs de l'application qui n'est reservée qu'à un usage interne. 
Nous proposons de mettre en place une fiche d'inscription qui sera gérée et créer par le chef des commerciaux et le chef du bureau d'étude afin de vérifier si la personne voulant s'inscrire à l'applicatif est admissible et identifiable. Cette fiche sera ensuite remis à l'administrateur qui créera le compte et donnera le bon rôle. Il communiquera ensuite les instructions, et notamment les identifiants et le premier mot de passe à changer lors de la première connexion. 

(A voir pour cette histoire de premier mot de passe, si on y a pensé et si non ce qu'on peut proposer)

### Gestion des périphériques BYOD

Les périphériques **BYOD** (Bring Your Own Device) utilisants l'application de Madera ne pourront y accéder uniquement par le biais d'un VPN interne à l'entreprise Madera afin de chiffrer les flux d'informations transitant entre le terminal BYOD et Madera. L'application sera accessible en **HTTPS** uniquement pour permettre le chiffrement par le biais d'un certificat d'authentification émit par l'utilisateur de Madera.

#### Mode hors-ligne

Madera fonctionne parfaitement en mode hors-ligne, pour se faire, nous utilisons **PWA** (Progressive Web App). Une **P**rogressive **W**eb **A**pp est une version **optimisée** d’un site mobile intégrant des fonctionnalités d’applications natives (normalement indisponibles sur un navigateur). Dans notre cas, nous utilisons cette technologie pour intégrer le support du mode hors-ligne à notre application Madera.

### Authentification avec JWT
Pour la sécurisation de l'authentification, nous avons sélectionné la technologie JWT.

#### JWT c'est quoi ?

JWT (JSON Web Token) est une norme ([RFC 7519](https://tools.ietf.org/html/rfc7519)) qui définit un moyen compact et autonome pour transmettre en toute sécurité des informations entre les parties en tant qu'objet JSON. Ces informations peuvent être vérifiées et fiables, car elles sont signées numériquement. Les JWT peuvent être signés à l'aide d'un secret (avec l'algorithme HMAC) ou d'une paire de clés publiques / privées à l'aide de RSA ou ECDSA.

Source : https://jwt.io/introduction/

Schéma fonctionnement JWT > https://wishtackblog.files.wordpress.com/2017/03/jwt-diagram.png

### Report de BUG et prise en charge de ceux ci

Les bugs seront reportés soit par les développeurs eux-mêmes lors du développement de l'application soit par les utilisateurs de l'application. Concernant les utilisateurs, lors de la découverte d'un bug, ils devront remplir un fiche de dépot de bug à envoyer a un administrateur du site.

(Fiche pour report de bug, voir desc l'issue)

Les bugs devront être corrigés en priorité lors du cycle de développement courant. Si le bug est dit "majeur" ou bloquant, il devra être traité en priorité et corrigé directement en environnement de production.

#### Utilisation de JWT dans notre application

Pour notre part, lors de l'authentification de l'utilisateur, le token est stocké dans les **cookies** du navigateur de l'utilisateur, ceci va permettre à l'application de vérifier si l'utilisateur a toujours un token non expiré pour accéder à celle-ci.

(Schéma d'intéraction front/back)

## Lister les issues associés à ce livrable

La liste des issues est détaillée dans trois tableaux différents ci-dessous :
- Premier tableau : Front-End
- Deuxième tableau : Back-End
- Troisième tableau : Rapport


Les tableaux sont décomposés en sept colonnes :
- Première colonne : Numéro du sprint
- Deuxième colonne : Numéro de l’issue
- Troisième colonne : description de l’issue
- Quatrième colonne : Niveau de difficulté de l’issue
- Cinquième colonne : Durée de réalisation de l’issue
- Sixième colonne : Coût de réalisation de l’issue
- Septième colonne : Ressource attribué à l’issue

Pour le Coût des issues, nous avons calculés le temps de développement par le taux horaire d’un développeur intermédiaire (expérimenté) ce basant sur 40000k brut annuel ( 31200 annuel net), soit 3333€ brut mensuel (2600 mensuel net), soit 22€ brut de l’heure (17 net de l’heure).
Exemple :
Coût du développeur à l’heure : 17€ net, soit 22€ brut.
Une heure de son temps coûte donc 22€ à l’entreprise.
Pour une tâche durant 2h par exemple, il coûtera 44€ à l’entreprise (taux horaire (22€) x temps de la tâche(2 heures) ).

## Conclusion
